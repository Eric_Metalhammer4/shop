"use strict";

const gulp = require('gulp');
const livereload = require('gulp-livereload');
const babel = require('gulp-babel');
const combiner = require('stream-combiner2');
const uglify = require('gulp-uglify');
const minifyCss = require("gulp-minify-css");
const minifyHtml = require("gulp-minify-html");

gulp.task('compressJS', function() {
    let combined = combiner.obj([
        gulp.src('scr/*.js'),
        babel({
            presets: ['es2015']
        }),
        uglify(),
        gulp.dest('public/javascripts')
    ]);
    combined.on('error', console.error.bind(console));

    return combined;
});
gulp.task('compressCss', function() {
    let combined = combiner.obj([
        gulp.src('scr/*.css'),
        minifyCss(),
        gulp.dest('public/stylesheets')
    ]);
    combined.on('error', console.error.bind(console));

    return combined;
});
gulp.task('compressHtml', function() {
    let combined = combiner.obj([
        gulp.src('scr/*.html'),
        minifyHtml(),
        gulp.dest('public')
    ]);
    combined.on('error', console.error.bind(console));

    return combined;
});
gulp.task('js', function() {
    gulp.src('./public/javascripts/*.js')
        .pipe(livereload());
});
gulp.task('css', function() {
    gulp.src('./public/stylesheets/*.css')
        .pipe(livereload());
});
gulp.task('html', function() {
    gulp.src('./public/*.html')
        .pipe(livereload());
});
gulp.task('watch', function() {
    livereload.listen();
    gulp.watch(['./public/javascripts/*.js'], ['js']);
    gulp.watch(['./public/stylesheets/*.css'], ['css']);
    gulp.watch(['./public/*.html'], ['html']);
});

gulp.task('default', ['watch']);
gulp.task('compress', ['compressCss', 'compressHtml', 'compressJS']);
