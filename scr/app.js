"use strict"
var app = angular.module('app', ['ngRoute']);

app.config(["$routeProvider", function($routeProvider) {
    $routeProvider
        .when('/index', {
            templateUrl: 'index.html',
            controller: 'indexCtrl'
        })
        .when('/shop/:typename', {
            templateUrl: 'shop.html',
            controller: 'typenameCtrl'
        })
        .otherwise({
            redirectTo: '/index'
        });
}]);
app.filter('unique', function() {
    return function(collection, keyname) {
        let output = [],
            keys = [];

        angular.forEach(collection, function(item) {
            let key = item[keyname];
            if (keys.indexOf(key) === -1) {
                keys.push(key);
                output.push(item);
            }
        });

        return output;
    };
});
app.filter("shopFilter", function() {
    return function(items, shop) {
        let results = [];
        let checkPush = false;
        if (items && shop)
            for (let i of items) {
                deleteNull(shop)
                if (custom_filter(i, shop))
                    results.push(i);
                checkPush = true;
            }

        return checkPush === false ? items : results;
    }
});
app.filter('propertiesFilter', function() {
    return function(input) {
        if (!input)
            return input;
        switch (input) {
            case "Текстиль":
                return "м";
                break;
            case "Посуда":
                return "л";
                break;
            case "Питание":
                return "г";
                break;
            default:
                return "";
                break;
        }
    }
})

function deleteNull(obj) {
    for (let i in obj)
        if (obj.hasOwnProperty(i))
            if (obj[i] === null)
                delete obj[i]
}

function custom_filter(item, obj) {
    for (let i in obj)
        if (obj.hasOwnProperty(i))
            switch (i) {
                case 'material':
                case 'color':
                    if (item[i] !== obj[i])
                        return false;
                    break;
                case 'cost_min':
                    if (item['cost'] < obj[i])
                        return false;
                    break;
                case 'properties_min':
                    if (item['properties'] < obj[i])
                        return false;
                    break;
                case 'cost_max':
                    if (item['cost'] > obj[i])
                        return false;
                    break;
                case 'properties_max':
                    if (item['properties'] > obj[i])
                        return false;
                    break;
                default:
                    return false;
                    break;
            }

    return true;
}
app.controller('indexCtrl', ["$scope", "$http", "$timeout", function($scope, $http, $timeout) {
    var timeOut;
    $http.get('/angular/slides').success((data, status) => {
        $scope.slides = data;
        $scope.$slideIndex = 0;
        $scope.play();
    }).error((err, status) => {
        console.log(err);
        console.log(status);
    });
    $scope.next = function() {
        $scope.$slideIndex = ++$scope.$slideIndex % $scope.slides.length;
    };
    $scope.play = function() {
        timeOut = $timeout(function() {
            $scope.next();
            $scope.play();
        }, 2000);
    };
}]);

app.controller('typenameCtrl', ["$scope", "$http", "$routeParams", function($scope, $http, $routeParams) {
    let typename = $routeParams.typename;
    $http.get('/angular/' + typename).success((data, status) => {
        $scope.models = data;
    }).error((err, status) => {
        console.log(err);
        console.log(status);
    });
}]);
