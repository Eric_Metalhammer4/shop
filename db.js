"use strict";

const Sequelize = require('sequelize');
let sequelize;
if (process.env.NODE_ENV === 'production') {
    sequelize = new Sequelize(process.env.DATABASE_URL, {
        pool: {
            max: 20,
            min: 0,
            idle: 10000
        },
        native: true
    });
} else {
    sequelize = new Sequelize('eric', 'postgres', '1164', {
        dialect: 'postgres'
    });
}

const Shop = sequelize.define('shops', {
    typename: Sequelize.TEXT,
    name: Sequelize.TEXT,
    material: Sequelize.TEXT,
    color: Sequelize.TEXT,
    cost: Sequelize.INTEGER,
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true
    },
    properties: Sequelize.FLOAT,
    image: Sequelize.TEXT
}, {
    timestamps: false,
    freezeTableName: true
});

class DB {
    read(typename) {
        return Shop.findAll({
            where: {
                typename: typename
            }
        });
    }
    slider() {
        return Shop.findAll({
            order: 'cost',
            limit: 10
        });
    }
}
module.exports = new DB();
