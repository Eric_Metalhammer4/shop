"use strict";

const restify = require('restify');
const db = require('./db');
const server = restify.createServer();
const NodeCache = require("node-cache");
const shopCache = new NodeCache();

server.get('/', restify.serveStatic({
    directory: './public',
    file: 'angular.html'
}));
server.get(/\/\w+.html/, restify.serveStatic({
    directory: './public'
}))
server.get(/\/javascripts\/.+.js/, restify.serveStatic({
    directory: './public'
}));
server.get(/\/stylesheets\/\w+.css/, restify.serveStatic({
    directory: './public'
}));
server.get('/favicon.ico', restify.serveStatic({
    directory: './public',
    file: 'favicon.ico'
}));

server.get('/angular/slides', (req, res, next) => {
    let value = shopCache.get("slides");
    if (value == undefined) {
        db.slider().then(results => {
            shopCache.set("slides", results);
            res.send(results);
        }).catch(err => {
            console.log(err);
            return next(err);
        })
    } else {
        res.send(value);
    }
});
server.get('/angular/:typename', (req, res, next) => {
    let typename = String(req.params.typename);
    let value = shopCache.get(typename);
    if (value == undefined) {
        db.read(typename).then(results => {
            shopCache.set(typename, results);
            res.send(results);
        }).catch(err => {
            return next(err);
        })
    } else {
        res.send(value);
    }
});

server.listen(process.env.PORT || '3000', function() {
    console.log('%s listening at %s', server.name, server.url);
});
